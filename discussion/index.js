// Function able to receive data without the use of global variables or prompt()

// "name" - is a parameter
// A parameter is a variable/container that exists only in our function and is used to store information that is provided to a function when it is called/invoked
function printName(name){
	console.log("My name is " + name);
};

// Data passed into a function invocation can be received by the function
// This is what we call an argument
printName("Jungkook");
printName("Thonie");

// Data passed into the function through funtion invocation is called arguments
// The argument is then stored within a container called a parameter
function printMyAge(age){
	console.log("I am " + age);
};

printMyAge(25);
printMyAge();

// check divisibility reusably using a function with arguments and parameter
function checkDivisibilityBy8(num){
	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is: " + remainder);

	let isDivisibleBy8 = remainder === 0;
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleBy8);
};

checkDivisibilityBy8(64);
checkDivisibilityBy8(27);
/*
	Mini-Activity

	1. Create a function which is capable to receive data as an argument:
		- This function should be able to receive the name of your favorite superhero
		- Display the name of your favorite superhero in the console
	 2. Create a function which is capable to receive a number as an argument:
	 	- This function should be able to receive a any number
	 	- Display the number and state if even number
 */

// Multiple Arguments can also be passed into a function; multiple parameters can contain our arguments

function printFullName(firstName, middleInitial, lastName){
	console.log(firstName + ' ' + middleInitial +' ' + lastName);
};

printFullName('Juan', 'Crisostomo', 'Ibarra');
printFullName('Ibarra', 'Juan', 'Crisostomo');

/*
	Parameters will contain the argument according to the order it was passed

	In other language, providing more/less arguments than the expected parameters sometime causes an errot or changes the behavior of the function

 */

printFullName('Stephen', 'Wardell');
printFullName('Stephen', 'Wardell', 'Curry', 'Thomas');
printFullName('Stephen', 'Wardell', 'Curry');
printFullName('Stephen', ' ', 'Curry');

// Use variables as arguments
let fName = "Larry";
let mName = "Joe";
let lName = "Bird";

printFullName(fName, mName, lName);

/* Mini - Activity - 10 mins
	Create a function which will be able to receive 5 arguments
		- Receive 5 of your favorite songs
		- Display/print the passed 5 favorite songs in the console when the function is invoked.

*/

function printMyTopSongs(song1, song2, song3, song4, song5){
	console.log("These are my favorite songs: ");
	console.log(song1);
	console.log(song2); 
	console.log(song3);
	console.log(song4);
	console.log(song5);

	//return song1 + song2 + song3 + song4 + song5;
	//return "Hello, sir Rupert";

};

printMyTopSongs("Pare ko", "Enter Sandman", "Balisong", "214", "Black Swan");

// Return Statement
// Currently or so far, our function are able to display data in our console
// However, our function cannot yet return values. Function are able to return values which can be saved into a variable using the return statement/keyword.
 let fullName = printFullName("Gabrielle", "Vhernadette", "Fernandez");
 console.log(fullName);//undefined

 function returnFullName(firstName, middleName, lastName){
 	return firstName + ' ' + middleName + ' ' + lastName; 
 };

fullName = returnFullName("Juan", "Ponce", "Enrile");
//printFullName();return undefined because the function does not have return statement
//returnFullName();return a string as a value becuase it has a return statement
console.log(fullName);

// Functions which have a return statement are able to return value and it can be saved in a variable
console.log(fullName + ' is my gradpa.');

function returnPhilippineAddress(city){
	return city + ", Philippines.";
}; 

// return - return a values from a function which we can save in a variable
let myFullAddress = returnPhilippineAddress("San Pedro");
console.log(myFullAddress);

// returns true if number is divisible by 4, else returns false
function checkDivisibilityBy4(number){
	let remainder = number % 4;//remainder = 0
	let isDivisibleBy4 = remainder === 0;// true = 0 === 0
	// console.log(isDivisibleBy4);//true 
	console.log(remainder);

	// returns either true or false
	// Not only can you returnm raw values/data, you can also directly return a value
	// console.log(isDivisibleBy4);
	return remainder;//0
	// return isDivisibleBy4;//true
	// return keyword not only allows us to return value but also ends the process of the function
	console.log("I am run after the return.");
};

let num4isDivisibleBy4 = checkDivisibilityBy4(4);//true
// let num14isDivisibleBy4 = checkDivisibilityBy4(14);
// num4isDivisibleBy4();
checkDivisibilityBy4(4);
console.log(checkDivisibilityBy4(4));//0
console.log(num4isDivisibleBy4);//true; 0
// console.log(num14isDivisibleBy4);

function createPlayerInfo(username, level, job){
	// console.log('username: ' + username + ' level ' + level + ' Job: ' + job);
	return('username: ' + username + ' level ' + level + ' Job: ' + job);
	console.log(1+1);
};

let user1 = createPlayerInfo('white_night', 95, 'Paladin');
console.log(user1);

/*
	Mini-Activity
	Create a function which will be able to add two numbers
		- numbers must be provided as argument
		- display the result of the addition in our console
		- function should only display result. It should not return anything.

		- invoke and pass 2 arguments to the addition function
		- use return

 */

function sumOfTwoNumbers(num1, num2){
	//console.log(num1 + num2);
	return num1 + num2;
};

let sum = sumOfTwoNumbers(5,2);
console.log(sum);

/*
	Create a function which will be able to multiply two numbers.
		- Numbers must be provided as arguments
		- Returm the result of multiplication

	Create a global variable called outside of the function called product
		- This product variable should be able to receive and store the result of multiplication function

	Log the value of product variable in the console

 */
 function multiplyNum(num1, num2){
	return num1 * num2;
};

let product = multiplyNum(7, 5);
console.log(product);

/*
  Create a function which wikk be able to get the area of a circle from a provided radius
     - a number should be provided as an argument
     - look up for the formula for calculating the area of a circle 
     - look up for the use of exponent operator
     - you can save the value of the calculations in a variable
     - return the result of the area 

   Create a global variable called outside of the function called circleArea
    - this variable should be able to receive and store the result of the calculation of area of a circle
   Log the value of the circleArea in the console.

 */
 
 function getCircleArea(radius){
	//** - exponent operator ^ 
	//A = pi R ^ 2
	/*let area = 3.1416 * (radius**2);
	return area;*/
	return 3.1416 * (radius**2);
};

let circleArea = getCircleArea(15);
console.log('The result of getting the aread of a circle is ' + circleArea);
console.log(circleArea);
 
/* =====================
 Assignment:
 
 Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console. */
